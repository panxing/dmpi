#include "DMPI/config.h"

#define GNTHOR_INTERNAL 1
#include "DMPI/sMPI.h"
//#include "gnthor/debug.h"
#include "DMPI/DMPI.h"
#include "DMPI/internal.h"
#include "DMPI/Reordering.h"

#include "mpi.h"

DMPI::internal::EState DMPI::internal::g_state = DMPI::internal::EO_ANALYSIS_ON;
extern DMPI::PeerQueue g_myqueue;
extern DMPI::Reordering myReordering;

//
int dmpi_initialize()
{
}

//
// start a communication region where we are to reorder
//
void dmpi_start_send_region()
{
// if we get here while pending, then that means there is no sync outside of
// the communication region, which is a performance bug
    assert(DMPI::internal::g_state != DMPI::internal::EO_COMPLETION_NEEDED);
    DMPI::internal::g_state = DMPI::internal::EO_ANALYSIS_ON;
}

//
// finish a communication region where we are to reorder
//
void dmpi_end_send_region()
{
// finish up issueing all outstanding requests, but the communication completion
// will come later when the client application calls it
//    if (gnthor::internal::issue_pending_requests()) {
//        gnthor::internal::g_state = gnthor::internal::EO_COMPLETION_NEEDED;
//    } else
    // nothing further to sync, so done
//        gnthor::internal::g_state = gnthor::internal::EO_ANALYSIS_OFF;
    assert(DMPI::internal::g_state == DMPI::internal::EO_ANALYSIS_ON); 
    myReordering.issue_request_bundled(g_myqueue);                                             
    DMPI::internal::g_state = DMPI::internal::EO_ANALYSIS_OFF;
}

//
// start a communication region where we are to reorder
//
void dmpi_start_recv_region()
{
// if we get here while pending, then that means there is no sync outside of
// the communication region, which is a performance bug
//    assert(gnthor::internal::g_state != gnthor::internal::EO_COMPLETION_NEEDED);
//    gnthor::internal::g_state = gnthor::internal::EO_ANALYSIS_ON;
}

//
// finish a communication region where we are to reorder
//
void dmpi_end_recv_region()
{
// finish up issueing all outstanding requests, but the communication completion
// will come later when the client application calls it
//    if (gnthor::internal::issue_pending_requests()) {
//        gnthor::internal::g_state = gnthor::internal::EO_COMPLETION_NEEDED;
//    } else
    // nothing further to sync, so done
//        gnthor::internal::g_state = gnthor::internal::EO_ANALYSIS_OFF;
}

/*TODO: rewrite into MPI
// run the analysis, after enough data is collected, immediately after waitsync
void gnthor_set_immediate_analysis()

// defer the analysis, after enough data is collected, into the next barrier
void gnthor_set_deferred_analysis()

// switch on/off analysis on (size, neighbor) pairs
void gnthor_set_keyed_analysis()

void gnthor_set_no_keyed_analysis()

// gnthor_print_stats(int) lives in callstats.cxx

// allow a (reorder-specific) "system reset"
int gnthor_reset()
*/
