#ifndef __GNTHOR_MPI_H__
#define __GNTHOR_MPI_H__

#ifdef __cplusplus
extern "C" {
#endif // ifdef __cplusplus
// public API with a few helper functions, usable from client code
	int dmpi_initialize();
	void dmpi_start_send_region();
	void dmpi_end_send_region();
	void dmpi_start_recv_region();
	void dmpi_end_recv_region();
#ifdef __cplusplus
}
#endif // ifdef __cplusplus

#endif // !__GNTHOR_MPI_H__

