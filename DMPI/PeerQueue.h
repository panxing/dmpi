#ifndef __GNTHOR_PEERQUEUE_H__
#define __GNTHOR_PEERQUEUE_H__

#include <mpi.h>
#include "datatypes.h"
#include "config.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <vector>
#include <assert.h>
#include <stddef.h>
#include <map>

#define BUNDLESIZE 1


namespace DMPI {

// queue of filed requests; for now, we use one queue per gasnet node
// so there is no domain mapping (and no domain_queue holding peers)
class PeerQueue {
public:
    typedef std::array<mem_req_t, REQ_QUEUE_LENGTH>::size_type size_type;   //define size_type

public:
    PeerQueue() {
    // m_head points to the one before the first unused slot (%length), m_tail_issued
    // to the last slot that was issued (%length; state == r_issued) and m_tail_completed
    // to the last completed request (%length) and is again potentially available
    //
    //  | ...              |
    //  | r_completed      |
    //  | ...              |
    //  | r_completed      |  <- m_tail_completed
    //  | r_issued         |
    //  | ...              |
    //  | r_issued         |  <- m_tail_issued
    //  | r_pending        |
    //  | ...              |
    //  | r_pending        |  <- m_head
    //  | 0 or r_completed |
    //
    // Numbers are only incremented: to get the slot, take %length. The
    // invariant that must hold is:
    //  m_tail_completed <= m_tail_issued <= m_head
    //
    // Note: the first slot will be skipped the first time around
        //TODO: initilze
	//m_head = m_tail_issued = m_tail_completed = 0;
        //m_requests[0].state = r_completed;
	num_requests[SEND_MESSAGE]=0;
	num_requests[RECV_MESSAGE]=0;
    }

public:
    void enqueue_request(int type, void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request);

    mem_req_t& get_request(size_type index) {
        return send_requests[index];
    }

//re-write the ready_for_issue function
    //bool ready_for_issue(int bundle_size){
   //	return bundle_size<=num_requests;
   //}

   bool ready_for_issue(int type){
        return BUNDLESIZE<=num_requests[type];
   }
// Get a bundle of indices of messages ready to be issued; after this call,
// the messages are assumed to be unavailable for other servers.
/*    bool ready_for_issue() {
        return m_head - m_tail_issued != 0;
    }
    bool ready_for_issue(int bundle_size) {
        if (0 < bundle_size && (m_head - m_tail_issued < (size_type)bundle_size))
            return false;
        return ready_for_issue();
    }
*/

//for a request buffer, bundle(target, id+1), since map has be initilzed by 0, so id has to add 1
    void get_bundle_for_issue(int bundle_size, int type, std::map<int, std::vector<int>>& bundle) {
        //bundle_size<0 means all the requests in buffer will be issued
	if (bundle_size < 0) {
            bundle_size = num_requests[type]; // i.e. all outstanding
        }
        //bundle.resize(bundle_size);
	//num_requests[type]=0;
	
	//std::vector<std::pair<int, int>> sorted_t; //id in buffer, target
        for (int i = 0; i < bundle_size; ++i){
		int target= type==SEND_MESSAGE? send_requests[i].DestSource : recv_requests[i].DestSource;
        	//bundle.push_back(std::make_pair(i, target));
		//bundle[target]=i+1;  //in this case, if bundle[target]==0, means this node will not communicate with this target
		bundle[target].push_back(i);
	}
    }

// Get a bundle of indices of messages that have either been handed out for issue
// (and may be still pending) or are issued and ready to be waited on; after this
// call, the messages are assumed to be unavailable for other servers.
//TODO: get_bundle_for_completion is the old gasnet code, should re-write in futurer.
/*    void get_bundle_for_completion(int bundle_size, std::vector<size_type>& bundle) {
        if (0 < bundle_size) {
            bundle_size = std::min(m_head - m_tail_completed, (size_type)bundle_size);
        } else {
            bundle_size = m_head - m_tail_completed;
            bundle.resize(bundle_size);
        }

        int rel_start = m_tail_completed + 1;
        m_tail_completed += bundle_size;
        assert(m_tail_completed <= m_tail_issued && m_tail_issued <= m_head);

        for (int ib = 0; ib < bundle_size; ++ib)
            bundle[ib] = (rel_start + ib) % REQ_QUEUE_LENGTH;

    }
*/

//private:
    std::array<mem_req_t, REQ_QUEUE_LENGTH> send_requests;   // queue of send requests
    std::array<mem_req_t, REQ_QUEUE_LENGTH> recv_requests;   // queue of receive requests
    volatile size_type num_requests[2];                      //0: SEND_MESSAGE, 1: RECV_MESSAGE

//TODO: Now I only how many messages in the m_requests queue, in future, m_head, m_tail_issued and m_tail_completed may need
    //volatile size_type m_head;                    // one before available slot (%length)
    //volatile size_type m_tail_issued;             // last issued request (%length)
    //volatile size_type m_tail_completed;          // last completed request (%length)
};
//TODO: add_request maybe need in future.
} // namespace DMPI

#endif // ! __GNTHOR_PEERQUEUE_H__
