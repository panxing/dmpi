#ifndef __GNTHOR_REORDERING_H__
#define __GNTHOR_REORDERING_H__

#include "datatypes.h"
#include "PeerQueue.h"
#include "mpi.h"

#include <algorithm>
#include <chrono>
#include <map>
#include <memory>
#include <vector>

#include <float.h>

namespace DMPI {

class AnalysisData {

public:
    AnalysisData(int type,  PeerQueue& queue){
	m_order_mapping.resize(queue.num_requests[type]);
        for (int ib=0;ib<queue.num_requests[type];ib++) {
            mem_req_t& mr = type==SEND_MESSAGE? queue.send_requests[ib] : queue.recv_requests[ib];
            m_order_mapping[ib] = std::make_pair(ib, mr.DestSource); //m_order_mapping[i].first is the order of messages, m_order_mapping[i].second is one message's target
        }
	m_state = EA_GLOBAL;
	m_best_average=FLT_MAX;
    }

public:
// current order
    //std::vector<std::pair<size_t, gasnet_node_t>> m_order_mapping;  //GASNet code
    std::vector<std::pair<size_t, int>> m_order_mapping;              //MPI code

// saved order in case of reversal (could recalculate, but this is
// easier than dealing with nodes that have fewer than max neighbors)
    //std::vector<std::pair<size_t, gasnet_node_t>> m_last_order_mapping; //GASNet code
    std::vector<std::pair<size_t, int>> m_last_order_mapping;             //MPI code

// measurements
//TODO: move Reordering into AnalysisData
/*    typedef std::vector<float>::size_type count_t;
    count_t m_skip;
*/
    typedef std::chrono::steady_clock clock_t;
    clock_t::time_point m_clock_start;
    std::vector<float> m_comm_times;

// history
    std::vector<std::vector<int>> m_orderings;
    std::vector<float> m_sd_times;           //the recoreded stddev for each node in the last iteration
    //std::vector<gasnet_node_t> m_order;    //GASNet code
    std::vector<int> m_order;                //MPI code
    float m_best_average;                    //The slowest time in last iteration
    size_t m_slowest_node;                   //The slowest non-edge node in last iteration

    enum AnalysisState { EA_GLOBAL, EA_LOCAL, EA_REMEASURE, EA_STOPPED };
    AnalysisState m_state;
};

// individual message posting reordering strategies

class Reordering {
public:
    virtual ~Reordering();

    virtual int Initialize();
    virtual int Reset(){};

    virtual bool Analyze() { return false; }

//TODO    
/*    virtual size_t issue_request_bundled(
        PeerQueue& queue, issue_policy_t policy, bool drain_queue) = 0;

    virtual size_t issue_bundle(
        PeerQueue& queue, const std::vector<PeerQueue::size_type>& bundle);
*/
    //virtual size_t request_completion(PeerQueue& queue);
    size_t issue_request_bundled(PeerQueue& queue);
//    size_t request_completion(MPI_Request *request, MPI_Status *status);
    size_t request_completion(int count, MPI_Request array_of_requests[],MPI_Status array_of_statuses[]);
    size_t issue_bundle(mem_req_t& cur_request, int type, int target);
    size_t Analysis(bool found);
public:
    int NUM_PARTNERS;
    int RankID;
    std::vector<float> latency;
    std::vector<MPI_Request*> measured_requests;
    std::vector<int> m_order;                //MPI code
    int warm_up;

// measurements , moved to AnalysisData
//    typedef std::chrono::steady_clock clock_t;
//    clock_t::time_point m_clock_start;

// measurements per (sum(size), sum(neighbors))
    typedef std::pair<size_t, size_t> AnalysisKey_t;
    AnalysisKey_t m_cur_key;
    std::map<AnalysisKey_t, AnalysisData> m_analysis_data;

//TODO
/*
public:
    static std::shared_ptr<Reordering> get() {
        static std::shared_ptr<Reordering> reordering = get_reordering();
        return reordering;
    }
*/
protected:
    std::vector<PeerQueue::size_type> m_bundle;

//TODO
/*
private:
    static std::unique_ptr<Reordering> get_reordering();
*/
};


//
// Instrument communication phase of fixed domain decomposition and reorder
// based on a global consensus
//TODO
/*
class GlobalSchedule : public Reordering {
    typedef AnalysisData::count_t count_t;
public:
    GlobalSchedule(int interval, int warmup, int remeasure) :
            m_interval((count_t)interval), m_warmup((count_t)warmup),
            m_remeasure((count_t)remeasure) {
    }

public:
    virtual int Initialize(){};
    virtual int Reset(){};

    virtual bool Analyze(){};

    virtual size_t issue_request_bundled(
        PeerQueue& queue, issue_policy_t policy, bool drain_queue);
    virtual size_t request_completion(PeerQueue& queue);

private:
// configuration
    const count_t m_interval, m_warmup, m_remeasure;

// measurements per (sum(size), sum(neighbors))
    typedef std::pair<size_t, size_t> AnalysisKey_t;
    AnalysisKey_t m_cur_key;
    std::map<AnalysisKey_t, AnalysisData> m_analysis_data;
};*/

} // namespace DMPI

#endif // ! __GNTHOR_REORDERING_H__
