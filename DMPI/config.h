#ifndef __GNTHOR_CONFIG_H__
#define __GNTHOR_CONFIG_H__

// TODO: set this to 65535, the maximum number of explicit gasnet handles in flight
// number is currently low just for debugging
#define REQ_QUEUE_LENGTH 65535


// Maximum number of measurements needed before skipping
//#define MAXOUT_MEASUREMENTS 10000


// Maximum number of communication rounds for global schedule reordering
// number is min(32 - HEADEROFF (== 3, see Reordering.cxx), 32 - 2x2 (2 floats,
// each being 2xsizeof(unsigned short)) - 1 (for size)), keeping the message
// below 64B in all cases
#define MAX_COMM_ROUNDS 40

//#define DEBUG_PRINT 1 

#endif // ! __GNTHOR_CONFIG_H__
