#ifndef __GNTHOR_DATATYPES_H__
#define __GNTHOR_DATATYPES_H__

#include "mpi.h"

#define SEND_MESSAGE 0
#define RECV_MESSAGE 1

//possible status of request
typedef enum {r_completed=123, r_pending=456, r_issued=789} req_state_t;


//TODO: the old gasnet code, should re-write or delete
/*
// issue policy for draining queue
typedef enum {all, only_aged, fixed_count, credit, timed} issue_policy_t;
*/

// individual request, re-write into MPI
typedef struct mem_req_struct {
   //gasnet_handle_t handle;

   void* 	 buf;
   size_t        size;
   int           DestSource;  //destination for send, source for receive
   MPI_Datatype  datatype;
   int           tag;
   MPI_Comm      comm;
   MPI_Request*  request;

   int           type;        // send or recieve
   volatile req_state_t state;       // processing state of request

   //char padding[24];                 // rounds to 64 bytes (TODO: verify)
} mem_req_t;

#endif // ! __GNTHOR_DATATYPES_H__
