#ifndef __GNTHOR_INTERNAl_H__
#define __GNTHOR_INTERNAL_H__

#include <string>
#include <vector>
#include <assert.h>
#include "sMPI.h"
//#include "DMPI/debug.h"
#include "datatypes.h"
#include "PeerQueue.h"



namespace DMPI {
namespace internal {

// state to allow message capture and reordering to be API-driven; this is needed in
// case the application has significant computation to overlap with communication
enum EState { EO_ANALYSIS_OFF, EO_ANALYSIS_ON, EO_COMPLETION_NEEDED };
extern EState g_state;

} //end of namespace internal
} //end of namespace DMPI


#endif // ! __GNTHOR_INTERNAL_H__
