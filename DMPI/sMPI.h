#ifndef __GNTHOR_SMPI_H__
#define __GNTHOR_SMPI_H__

#include <mpi.h>

#ifndef GNTHOR_INTERNAL

#warning GNTHORMPI is redefining mpi APIs

#ifdef MPI_Comm_size
#undef MPI_Comm_size
#endif
#define MPI_Comm_size             sMPI_Comm_size

#ifdef MPI_Comm_rank
#undef MPI_Comm_rank
#endif
#define MPI_Comm_rank             sMPI_Comm_rank

#ifdef MPI_Isend
#undef MPI_Isend
#endif
#define MPI_Isend             sMPI_Isend

#ifdef MPI_Irecv
#undef MPI_Irecv
#endif
#define MPI_Irecv             sMPI_Irecv

#ifdef MPI_Wait
#undef MPI_Wait
#endif
#define MPI_Wait             sMPI_Wait

#ifdef MPI_Waitall
#undef MPI_Waitall
#endif
#define MPI_Waitall             sMPI_Waitall

#ifdef MPI_Barrier
#undef MPI_Barrier
#endif
#define MPI_Barrier             sMPI_Barrier

#endif //!GNTHOR_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif // ifdef __cplusplus

	//int sMPI_Init( int *argc, char ***argv );
	int sMPI_Comm_size( MPI_Comm comm, int *size );
	int sMPI_Comm_rank( MPI_Comm comm, int *rank );
	int sMPI_Isend(const void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request);
	int sMPI_Irecv(void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request *request);
	int sMPI_Wait(MPI_Request *request, MPI_Status *status);
	int sMPI_Waitall(int count, MPI_Request array_of_requests[],MPI_Status array_of_statuses[]);	
	int sMPI_Barrier( MPI_Comm comm );

#ifdef __cplusplus
}
#endif // ifdef __cplusplus

#endif // !__GNTHOR_SMPI_H__

