IDIR =DMPI

CC=mpic++
CFLAGS=-I$(IDIR) -std=c++11

#add .h files here
_DEPS = DMPI.h sMPI.h datatypes.h PeerQueue.h Reordering.h config.h internal.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

#add .cxx files here
OBJ = hello.o DMPI.o sMPI.o PeerQueue.o Reordering.o 

%.o: %.cxx $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

hello.exe: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f *.exe *.o *~ core $(INCDIR)/*~
