//#include "gnthor/config.h"
#define GNTHOR_INTERNAL 1
//#include "gnthor/debug.h"
//#include "gnthor/internal.h"
#include "DMPI/PeerQueue.h"
#include "DMPI/Reordering.h"
#include <assert.h>

extern DMPI::Reordering myReordering;
//
// Queue message request. Both gets and puts are entered in the same queue (just
// distinguished by request type).
//0:SEND_MESSAGE, 1:RECV_MESSAGE
//TODO: should there be some return value????
//TODO: add "type" arugument
void DMPI::PeerQueue::enqueue_request(int type, void *buf, int count, MPI_Datatype datatype, int dest_source, int tag, MPI_Comm comm, MPI_Request *request)
{
    //GNTHOR_TRACER(2);  TODO: trace function
    //uint64_t slot = num_requests+1;
    int slot=num_requests[type];
    mem_req_t& cur_request = type==SEND_MESSAGE? send_requests[slot] : recv_requests[slot];
    num_requests[type]+=1;
    //printf("num_requests of type %d = %ld\n", type, num_requests[type]);

    cur_request.buf = buf;
    cur_request.size = count;
    cur_request.DestSource = dest_source;
    cur_request.datatype = datatype;
    cur_request.tag = tag;
    cur_request.comm = comm;
    cur_request.request = request;

    cur_request.type = type;
    cur_request.state = r_pending;

//Only add request into buffer, issue all the requests in MPI_Wait/MPI_Waitall
//    if (ready_for_issue(type))
//   	myReordering.issue_request_bundled(*this);

// TODO: Do MPI need synchronize here??
//    __sync_synchronize();
}
