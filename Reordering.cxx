#include "DMPI/config.h"

#define GNTHOR_INTERNAL 1
#include "DMPI/sMPI.h"
#include "DMPI/Reordering.h"
//#include "gnthor/system_stats.h"
//#include "gnthor/callstats.h"

#include"mpi.h"

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <utility>
#include <vector>
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define REORDERING 1
//#define Sort_by_Target 1

// set the rank that performs the analysis (TODO: should be the fastest
const int ANALYSIS_RANK = 2;               //MPI code
const int MEASURE_TIMES = 100;
const int WARM_UP = 10;

//MPI code, only for GlobalSchedule
/*//TODO
std::unique_ptr<DMPI::Reordering> DMPI::Reordering::get_reordering()
{   
    std::unique_ptr<Reordering> reordering = nullptr;
    
    //const char* coption = getenv("GNTHOR_GLOBAL_DECISION_INTERVAL");
    //int interval = coption ? atoi(coption) : 100;
    int interval=100;
    //coption = getenv("GNTHOR_GLOBAL_DECISION_WARMUP");
    //int warmup_skip = coption ? atoi(coption) : 100;
    int warmup_skip=100;
    //coption = getenv("GNTHOR_GLOBAL_DECISION_REMEASURE");
    //int remeasure = coption ? atoi(coption) : 1000;
    int remeasure=1000;
        //if (!gasnet_mynode()) {
        //   std::cerr << "[gnthor] Analysis interval:  " << interval    << std::endl;
        //   std::cerr << "[gnthor] Analysis warmup:    " << warmup_skip << std::endl;
        //   std::cerr << "[gnthor] Analysis remeasure: " << remeasure   << std::endl;
        //}
//    reordering = std::unique_ptr<Reordering>(new GlobalSchedule{interval, warmup_skip, remeasure});
    return reordering;
}*/

// base class
DMPI::Reordering::~Reordering() {}

//
// factor out issueing a bundle of messages
size_t DMPI::Reordering::issue_bundle(mem_req_t& cur_request, int type, int target)
{
	if (type==SEND_MESSAGE){
                measured_requests.push_back(cur_request.request);
		MPI_Isend(cur_request.buf, cur_request.size, cur_request.datatype, cur_request.DestSource, cur_request.tag, cur_request.comm, cur_request.request);
        }
        else{ //type=RECV_MESSAGE
		//measured_requests.push_back(*cur_request.request);
                MPI_Irecv(cur_request.buf, cur_request.size, cur_request.datatype, cur_request.DestSource, cur_request.tag, cur_request.comm, cur_request.request);
        }
}

//Current, this function only issue send message, since recv message is issued normally (NO Reordering for recv message)
size_t DMPI::Reordering::issue_request_bundled(PeerQueue& queue)
{
    //find the AnalysisData with key(ranks, size)
    size_t sum_size = 0, sum_ranks = 0;
    for (int ib=0; ib<queue.num_requests[SEND_MESSAGE]; ib++ ) {
        mem_req_t& mr = queue.send_requests[ib];
        sum_size  += mr.size;
        sum_ranks += mr.DestSource;
    }
    m_cur_key = AnalysisKey_t{sum_size, sum_ranks};
    auto pit = m_analysis_data.find(m_cur_key);
    if (pit == m_analysis_data.end()) {
//	printf("This is a new key for Rank %d, sum_size=%d, sum_ranks=%d\n", RankID, sum_size, sum_ranks);
        auto res = m_analysis_data.emplace(m_cur_key, AnalysisData{SEND_MESSAGE,queue});
        assert(res.second);  //check whether insert successfully
        pit = res.first;     //get the iterator
    }
    else{
/*	AnalysisData& ana2 = pit->second;
	for (int i=0;i<ana2.m_order_mapping.size();i++)
		printf("In Rank %d, message %d is sent to rank %d\n",RankID, ana2.m_order_mapping[i].first, ana2.m_order_mapping[i].second);
*/
    }	
    AnalysisData& ana = pit->second;
 
    ana.m_clock_start = AnalysisData::clock_t::now();  
 
    //Next, issue send request
    /*Issue send request beased on Request*/
    // reorder bundle according to neighbor schedule (note: since reordering starts from the very first schedule, this enforces that particular starting order, even if the application does not)
    std::vector<PeerQueue::size_type> reordered; reordered.resize(queue.num_requests[SEND_MESSAGE]);
    for (size_t i = 0; i < queue.num_requests[SEND_MESSAGE]; ++i) {
        assert(ana.m_order_mapping[i].first < reordered.size());
        reordered[ana.m_order_mapping[i].first] = i;
    }
#ifdef DEBUG_PRINT
    if (RankID==0){
    	printf("Rank %d's reordered is ", RankID);
    	for (int i=0;i<reordered.size();i++)
    		printf("%d, ", reordered[i]);
    	printf("\n");

        printf("Here is rank %d, my final order of m_order_mappingis ", RankID);
        for (int i=0;i<ana.m_order_mapping.size();i++)
        printf("%d,", ana.m_order_mapping[i].first);
        printf("\n");
    }
#endif

    if (queue.num_requests[SEND_MESSAGE]!=0){
	//assert(queue.num_requests[SEND_MESSAGE] = reordered.size());
    	for (int i=0;i<reordered.size();i++){
    		mem_req_t& cur_request = queue.send_requests[reordered[i]]; 
#ifdef DEBUG_PRINT
        	printf("Rank %d sends %dth message to Rank %d\n", RankID, i, cur_request.DestSource);
#endif
		issue_bundle(cur_request, SEND_MESSAGE, cur_request.DestSource);
        	cur_request.state = r_issued;
    	}
    	queue.num_requests[SEND_MESSAGE]=0;  //maybe should not clear
    }

    /*End of Issue send request beased on Request*/

#ifdef Sort_by_Target
    /*Issue send request beased on Target*/    
    std::map<int, std::vector<int>> send_bundle;
    queue.get_bundle_for_issue(-1, SEND_MESSAGE, send_bundle);
   
//Here, m_bundle is map<int, vector<int>>, it can solve multiple message between source and target
    for (int i=0;i<m_order.size();i++){
        //mem_req_t& cur_request = queue.send_requests[i];
	int target=m_order[i];
	if (send_bundle[target].empty()){   //determine whether there is message between source and target
//		printf("There is no message between rank %d and rank %d\n", RankID, target);
		continue;
	}
	else{
		for (auto i:send_bundle[target]){
			mem_req_t& cur_request = queue.send_requests[i];  //since DMPI use 0 to indicate there is no message
			//printf("For rank %d, request %d send to rank %d, and the DestSource=%d\n", RankID, i, target, cur_request.DestSource);
			issue_bundle(cur_request, SEND_MESSAGE, cur_request.DestSource);
        		cur_request.state = r_issued;
		}
	}
    }
    send_bundle.clear(); 
    queue.num_requests[SEND_MESSAGE]=0;  //maybe should not clear
    /*End of Issue send request beased on Target*/
#endif

}

size_t DMPI::Reordering::request_completion(int count, MPI_Request array_of_requests[],MPI_Status array_of_statuses[])
{
	bool found=false;
	for (int i=0;i<count;i++){
		if (std::find(measured_requests.begin(), measured_requests.end(),&array_of_requests[i])!=measured_requests.end()){
			found=true;
			measured_requests.clear();  //TODO: only erase array_of_request+count
#ifdef DEBUG_PRINT	
			//printf("Rank %d find a send request\n", RankID);
#endif		
			break;
		}
#ifdef DEBUG_PRINT
		//else
		//	printf("Not found\n");
#endif
	}
	MPI_Waitall(count, array_of_requests,array_of_statuses);
	if (found){
		auto pit = m_analysis_data.find(m_cur_key);
    		assert(pit != m_analysis_data.end());
    		AnalysisData& ana = pit->second;
		std::chrono::duration<double>lapsed_time = AnalysisData::clock_t::now() - ana.m_clock_start;
#ifdef DEBUG_PRINT        	
		//printf("The latency of Rank %d is %f\n", RankID, lapsed_time.count());
#endif		
		if (warm_up==0)
			ana.m_comm_times.push_back(lapsed_time.count()*1000*1000);
		warm_up = warm_up>0? warm_up-1: 0;
		
		//move this part into sMPI_Barrier and analysis()
		//if (MEASURE_TIMES<=ana.m_comm_times.size())   //TODO:make it better
                //	Analysis(true);
	}
}

int DMPI::Reordering::Initialize(){
	warm_up=WARM_UP;
	//latency.resize(NUM_PARTNERS);
//	m_clock_start.resize(NUM_PARTNERS);
	//initilze an order
	m_order.resize(NUM_PARTNERS);
	for (int i=0;i<NUM_PARTNERS;i++){
        	m_order[i]=i;
        }
}

static bool pairCompare(const std::pair<double, int*>& firstElem, const std::pair<double, int*>& secondElem) {
  return firstElem.first < secondElem.first;
}

size_t DMPI::Reordering::Analysis(bool found){
	bool need_reversal;

	auto pit = m_analysis_data.find(m_cur_key);
        assert(pit != m_analysis_data.end());
        AnalysisData& ana = pit->second;

	//determine whether should do analysis
	if (MEASURE_TIMES>ana.m_comm_times.size())
		return -1;

	//calucate average and stddev
	float aveg_latency=0;
	for (int i=0;i<ana.m_comm_times.size();i++)
		aveg_latency+=ana.m_comm_times[i];
	aveg_latency = aveg_latency/ana.m_comm_times.size();
        float stddev = 0.;
        for (auto val : ana.m_comm_times) {
            float s = val - aveg_latency;
            stddev += s*s;
        }
        double divisor = ana.m_comm_times.size() == 1 ? 1 : ana.m_comm_times.size() - 1;
        stddev = sqrt(stddev/divisor);
        
	ana.m_comm_times.clear();

	//send the average time and stddev
	float send_buf[2];
	send_buf[0]=aveg_latency; send_buf[1]=stddev;
#ifdef DEBUG_PRINT
	//printf("In rank %d, aveg_latency=%f and stddev=%f\n", RankID, aveg_latency, stddev);
#endif	
	float *rbuf = NULL;
	if (RankID==ANALYSIS_RANK)
		rbuf = (float*) malloc(sizeof(float) * NUM_PARTNERS*2);	
	MPI_Gather(send_buf, 2, MPI_FLOAT, rbuf, 2, MPI_FLOAT, ANALYSIS_RANK, MPI_COMM_WORLD); 
	
	//send the m_order_mapping
	int *sbuf=(int*) malloc(sizeof(int)*MAX_COMM_ROUNDS);
	for (size_t i=0;i<MAX_COMM_ROUNDS;i++){
		if (i < ana.m_order_mapping.size())
			sbuf[ana.m_order_mapping[i].first]=ana.m_order_mapping[i].second;
		else
			sbuf[i]=-1;  //mean NULL
	}
	int *rbuf_order=NULL;
	if (RankID==ANALYSIS_RANK)
		rbuf_order=(int*) malloc(sizeof(int)*MAX_COMM_ROUNDS*NUM_PARTNERS);			
	MPI_Gather(sbuf, MAX_COMM_ROUNDS, MPI_INT, rbuf_order, MAX_COMM_ROUNDS, MPI_INT, ANALYSIS_RANK, MPI_COMM_WORLD);
	
	//TODO: here should not determine only by root's found flag, should do a gather of found flag 
	if (found){
		if (RankID==ANALYSIS_RANK){
			size_t max_neighbors = 0;   //record the non-edge node's request number //TODO Question 
			
			//calucate the average time among entire MPI common
			float entire_average=0;
                        for (int i=0;i<NUM_PARTNERS;i++)
                                entire_average+=rbuf[2*i];
			
			/*Sort by Request*/
			// collect all info
            		std::vector<float> avg_times; avg_times.resize(NUM_PARTNERS);
            		std::vector<float> read_sd_times; read_sd_times.resize(NUM_PARTNERS);  //TODO
			for (int i=0;i<NUM_PARTNERS;i++){
				avg_times[i]=rbuf[2*i];
				read_sd_times[i]=rbuf[2*i+1];
			}

#ifdef DEBUG_PRINT
			for (int i=0;i<NUM_PARTNERS;i++){
                                printf("I am root, the latency of rank %d is %f us\n", i, avg_times[i]);
				printf("I am root, the stddev of rank %d is %f us\n", i, read_sd_times[i]);
			}
#endif
			
			//split the received order, after MPI_Gather, all the sbuf will be combined into an array (rbuf_order) together. 
			//Here, use MAX_COMM_ROUNDS to split rbuf_order. MAX_COMM_ROUNDS is defined in config.h
			std::vector<std::vector<int>> read_orderings; read_orderings.resize(NUM_PARTNERS);
			for (int i=0;i<NUM_PARTNERS;i++){
				int j=0;
				while(rbuf_order[i*MAX_COMM_ROUNDS+j]!=-1 && j<MAX_COMM_ROUNDS){
					read_orderings[i].push_back(rbuf_order[i*MAX_COMM_ROUNDS+j]);
					j++;
				}
				max_neighbors = std::max(max_neighbors, read_orderings[i].size());
			}

			// check for slowest (needed for history)
            		size_t slowest_node = (size_t)-1; float slowest_time = -1.f;
            		for (size_t i = 0; i < avg_times.size(); ++i) {
                		if (slowest_time < avg_times[i]) {
                    			slowest_time = avg_times[i];
                    			slowest_node = i;
                		}
            		}
			printf("The slowest node is %d and its latency is %f\n", slowest_node, slowest_time);

			//check whether DMPI gets improvement in this interation
			if (slowest_time < ana.m_best_average){     //we improved, reorder based on average time
			//if (1){                                        //ONLY use Global Analysis
				if (ana.m_state == AnalysisData::EA_LOCAL)
					ana.m_state = AnalysisData::EA_GLOBAL;
				assert(ana.m_state == AnalysisData::EA_GLOBAL);
				ana.m_best_average = slowest_time;

				//copy the orders into root's AnalysisData
                        	ana.m_orderings = std::move(read_orderings);

#ifdef DEBUG_PRINT
                        	/*for (int i=0;i<NUM_PARTNERS;i++){
                                	printf("The size of Rank %d order is %d\n", i, ana.m_orderings[i].size());
                                	printf("Here is the root, The  received order of rank %d is ", i);
                                	for (int j=0;j<ana.m_orderings[i].size();j++)
                                        	printf("%d, ",ana.m_orderings[i][j]);
                                	printf("\n");
                        	}*/
#endif

				// average times are always current (no save)

            			// save sd_times for possible use next round, as well as the
            			// currently slowest non-edge node
                		ana.m_sd_times = std::move(read_sd_times);

				float non_edge_slowest_time = -1.f;
                		for (size_t i = 0; i < avg_times.size(); ++i) {
                    			if (non_edge_slowest_time < avg_times[i] && ana.m_orderings[i].size() == max_neighbors) {
                        			non_edge_slowest_time = avg_times[i];
                        			ana.m_slowest_node = i;
                    			}
                		}
			}
			else{   //NO improve, reorder based on stddev
				if (ana.m_state == AnalysisData::EA_GLOBAL) {
		                	ana.m_state = AnalysisData::EA_LOCAL;
                		// use stddevs from previous round for this analysis (no change)
                		// likewise, restore old order for analysis (no change)
                		} else if (ana.m_state == AnalysisData::EA_LOCAL) {
                    			ana.m_state = AnalysisData::EA_STOPPED;
                		// restore old order (no change)
                		} else
                    			assert(0);
//#ifndef NDEBUG
                		//TODO
				read_orderings.clear();
                		read_sd_times.clear();
//#endif
			}
			

			need_reversal = false;
			if (ana.m_state == AnalysisData::EA_GLOBAL){
				// calculate weighted sums; allow only the slowest 50% of nodes to vote; exclude
            			// nodes that do not have a full complement of communication partners
				//votes[i].first = time, votes[i].second = order
				//max_neighbors is the maximum requests number among all nodes. 
				//Since DMPI implements on All to All communication, all non-edge nodes have send same number of messages
#ifdef DEBUG_PRINT				
				printf("max_neighbor=%d\n",max_neighbors);
#endif                		
				std::vector<std::pair<float, size_t>> votes; votes.resize(max_neighbors);
                		for (size_t i = 0; i < votes.size(); ++i) {
                    			votes[i].first  = 0.f;
                    			votes[i].second = i;
                		}

                		std::vector<std::pair<float, size_t>> sorted_t; sorted_t.reserve(avg_times.size());
                		for (size_t i = 0; i < avg_times.size(); ++i){
                    			sorted_t.push_back(std::make_pair(avg_times[i], i));
				}
                		std::sort(sorted_t.rbegin(), sorted_t.rend());
		
                		for (size_t i = 0; i < sorted_t.size(); ++i) {         //TODO sorted_t.size()/2 ?????????????
                    			size_t voting_node = sorted_t[i].second;      //The top 50% of the slowest nodes
                    			const auto& order = ana.m_orderings[voting_node];  //One node's order
                    			if (order.size() == max_neighbors) {             //check it is not the edge node
                        			for (size_t iround = 0; iround < max_neighbors; ++iround) {
                            				assert(order[iround] < avg_times.size());
                            				votes[iround].first += avg_times[order[iround]];  //votes[i].first = total latency of ith requests among all nodes, votes[i].second=i;
						}
                    			}	
                		}
		
				// sort rounds by weighted value, sort by votes[i].first
                		std::sort(votes.rbegin(), votes.rend());

            			// save new order, In Recording.h, vector<vector<int>> m_orderings; vector<int> m_order; 
                		ana.m_order.clear(); ana.m_order.reserve(MAX_COMM_ROUNDS);
                		//After sort, if m_order[i]=j, i is new order for all requests, j is one request's ID in old order. 
				//E.g., m_order[0]=3 means, the 3th requests in old order should be issued firstly in the next iteration
				for (auto& v : votes)
                    			ana.m_order.push_back(v.second);
				int iter=ana.m_order.size();
				for(int i=iter;i<MAX_COMM_ROUNDS;i++)
					ana.m_order.push_back(-1);			

				//printf("On the root, the size of m_order is %d, and MAX_COMM_ROUNDS=%d, iter=%d\n", ana.m_order.size(), MAX_COMM_ROUNDS, iter);
				printf("RESULT %f, and The new order is ", entire_average);
				for (int i=0;i<ana.m_order.size();i++)
					printf("%d, ",ana.m_order[i]);  //print the request ID
				printf("\n");	
				/*End of Sort by Request*/

						
#ifdef Sort_by_Target
				/*Sort by Target*/
				printf("Now, DMPI sort message order by target\n");
				std::vector<std::pair<float, int>> sorted_Target; sorted_Target.reserve(NUM_PARTNERS);
                		for (int i = 0; i < NUM_PARTNERS; ++i)
                    			sorted_Target.push_back(std::make_pair(rbuf[i], i));
                		std::sort(sorted_Target.rbegin(), sorted_Target.rend());
				for (int i=0;i<sorted_Target.size();i++){
					//printf("The %dth is rank %d and its latency is %f us\n", i+1, sorted_t[i].second, sorted_t[i].first);		
					m_order[i]=sorted_Target[i].second;
				}
				printf("RESULT %f and the next final order is ", entire_average);
				for (int i=0;i<m_order.size();i++)
					printf("%d ", m_order[i]);
				printf("\n");
				/*End of Sort by Target*/
#endif
			}
			else if(ana.m_state == AnalysisData::EA_LOCAL){
				printf("We donot get improvement this iteration, use last iteration's stddev\n");
			        // retrieve order of slowest node and sort
                		const auto& slowest_order = ana.m_orderings[ana.m_slowest_node];   //the collected order in the last iteration
				assert(slowest_order.size() == max_neighbors);                     //because DMPI works on ALL to ALL moduel
          			// vote based on standard deviation
                		std::vector<std::pair<float, size_t>> votes;
                		votes.resize(slowest_order.size());
                		for (size_t iround = 0; iround < slowest_order.size(); ++iround) {
                   			assert(slowest_order[iround] < ana.m_sd_times.size());
                   			votes[iround].first = ana.m_sd_times[slowest_order[iround]];
                   			votes[iround].second = iround;   //TODO error!!!! should be target not iround
                		}

#ifdef DEBUG_PRINT
				for (int i=0;i<votes.size();i++)
					printf("votes[%d].first=%f, votes[%d].second=%d\n", i, votes[i].first, i, votes[i].second);
#endif

            			// sort and save
                		std::sort(votes.rbegin(), votes.rend());

            			// we worked on info from a previous round, so require a reversal before
            			// applying new order
                		need_reversal = true;

            			// save new order
                		ana.m_order.clear(); ana.m_order.reserve(votes.size());
                		for (auto& v : votes)
                    			ana.m_order.push_back(v.second);
				int iter=ana.m_order.size();
                                for(int i=iter;i<MAX_COMM_ROUNDS;i++)
                                        ana.m_order.push_back(-1);

				printf("RESULT %f, and The new LOCAL order is ", entire_average);
                                for (int i=0;i<ana.m_order.size();i++)
                                        printf("%d, ",ana.m_order[i]);  //print the request ID
                                printf("\n"); 
			}
			else if (ana.m_state == AnalysisData::EA_STOPPED) {
                		if (ana.m_best_average<= slowest_time) {
                		// got here b/c of overshoot: restore order

                    		need_reversal = true;

                		// the reversal restores; for simplicity, apply a no-op reorder
                    		assert(ana.m_order.size() == MAX_COMM_ROUNDS);
                    		for (int i = 0; i < max_neighbors; ++i)
                        		ana.m_order[i] = i;
				for (int i=max_neighbors;i<MAX_COMM_ROUNDS;i++)
                                        ana.m_order[i] = -1;

				//TODO: add EA_REMEASURE
				ana.m_state = AnalysisData::EA_GLOBAL;
				ana.m_best_average=FLT_MAX;
				printf("Since Analysis STOPPED, Remeasure!\n");
                		} else
                    			//if (GNTHOR_VERBOSE) std::cerr << "[gnthor:" << ANALYSIS_RANK << "] Analysis STOPPED\n";
					printf("DMPI: %d Analysis STOPPED\n", RankID);
            		} else {
                		assert(0);
            		}
		}

		
		/*Broadcast the Sorted Order by Request*/
		if (RankID!=ANALYSIS_RANK){
			ana.m_order.clear(); 
			ana.m_order.resize(MAX_COMM_ROUNDS);
		}
		MPI_Bcast(&ana.m_order[0], MAX_COMM_ROUNDS, MPI_INT, ANALYSIS_RANK, MPI_COMM_WORLD);
#ifdef DEBUG_PRINT		
		//printf("Here is rank %d, my final order is ", RankID);
                //for (int i=0;i<ana.m_order.size();i++)
                //        printf("%d,", ana.m_order[i]);
                //printf("\n");
		//MPI_Barrier(MPI_COMM_WORLD );	
#endif

		/*Broadcast need_reversal*/
		int reversal=-1;
		if (RankID==ANALYSIS_RANK)
			reversal = (int) need_reversal;
		MPI_Bcast(&reversal, 1, MPI_INT, ANALYSIS_RANK, MPI_COMM_WORLD);

#ifdef DEBUG_PRINT
		if (RankID==1)
			printf("In Rank %d, reversal=%d\n", RankID, reversal);
#endif		

		if (reversal){
			//assert(!ana.m_last_order_mapping.empty());
            		ana.m_order_mapping = ana.m_last_order_mapping;
#ifdef DEBUG_PRINT
			if (RankID==1)
				for(int i=0;i<ana.m_order_mapping.size();i++)
					printf("REVERSAL m_order_mapping[%d].first=%d, m_order_mapping[%d]second=%d\n", i, ana.m_order_mapping[i].first, i, ana.m_order_mapping[i].second);
#endif		
		}
		else {
			ana.m_last_order_mapping = ana.m_order_mapping;
#ifdef DEBUG_PRINT
			if (RankID==1)
                                for(int i=0;i<ana.m_order_mapping.size();i++)
                                        printf("m_order_mapping[%d].first=%d, m_order_mapping[%d]second=%d\n", i, ana.m_order_mapping[i].first, i, ana.m_order_mapping[i].second);
#endif		
		}	
		//Move the m_order into m_order_mapping. In Reordering.h, vector<pair<size_t, int>> m_order_mapping; 
		//TODO: Here should be optimize. Consider and solve the -1 bits in m_order 
		for (auto& p : ana.m_order_mapping) {
                	size_t ioffset = 0;
			//TODO: here can be optimize. Do not need to search MAX_COMM_ROUNDS
                	for (size_t i = 0; i < MAX_COMM_ROUNDS; ++i) {
                    		size_t newpos = ana.m_order[i];
                    		if (p.first == newpos) {
                        		p.first = i;
                        		break;
                    		}
                	}
            	}
#ifdef DEBUG_PRINT
		printf("Here is rank %d, my final order of m_order_mappingis ", RankID);
               	for (int i=0;i<ana.m_order_mapping.size();i++)
               	        printf("%d,", ana.m_order_mapping[i].first);
               	printf("\n");
               	//MPI_Barrier(MPI_COMM_WORLD );
#endif 
		/*End of Broadcast the Sorted Order by Request*/

#ifdef Sort_by_Target		
		//Broadcast the Sorted Order by Target 
		//m_order.resize(NUM_PARTNERS);
#ifdef REORDERING		
		MPI_Bcast(&m_order[0], NUM_PARTNERS, MPI_INT, ANALYSIS_RANK, MPI_COMM_WORLD);
#endif
		/*printf("Here is rank %d, my final order is ", RankID);
		for (int i=0;i<m_order.size();i++)
			printf("%d,", m_order[i]);
		printf("\n");*/
		latency.clear();
		//End of Broadcast the Sorted Order by Target
#endif 
	}
	return 1;
}

//
// initialization for global schedule
//TODO: not sure whether it is needed in MPI/
/*
int gnthor::GlobalSchedule::Initialize()
{
    //GNTHOR_TRACER(1);
    gnthor::internal::g_issue_policy = gnthor::internal::EI_DRAIN_ONLY;
    return 0;
}*/

//
// reset ordering to the next application provided one and redo analysis
/*
int DMPI::GlobalSchedule::Reset()
{
    //GNTHOR_TRACER(1);

    m_analysis_data.clear();

    return 0;
}
*/
