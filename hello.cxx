#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <chrono>

#include "DMPI/sMPI.h"
#include "DMPI/DMPI.h"

//#define DEBUG_PRINT 1

#define WARM_UP 10
#define NUM_ITERATIONS 2000+WARM_UP 
#define NUM_PARTNERS 4   //4 ranks, 0,1,2,3
#define MESSAGE_SIZE  256*1024  //1M=256K * sizeof(int)
/************************************************************
This is a simple isend/ireceive program in MPI
************************************************************/

int main(int argc, char** argv)
{
    int myid, numprocs;
    int tag_1,tag_2,source,destination,count;
    //int r_buffer[NUM_PARTNERS];
    //MPI_Status s_stat[NUM_PARTNERS], r_stat[NUM_PARTNERS];
    //MPI_Request s_request[NUM_PARTNERS],r_request[NUM_PARTNERS];
 
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);

    MPI_Status s_stat[NUM_PARTNERS*2], r_stat[NUM_PARTNERS*2];
    MPI_Request s_request[NUM_PARTNERS*2],r_request[NUM_PARTNERS*2];
    int buffer_1[MESSAGE_SIZE];
    int buffer_2[MESSAGE_SIZE];
    int r_buffer_1[MESSAGE_SIZE];
    int r_buffer_2[MESSAGE_SIZE];

    for (int i=0;i<NUM_PARTNERS*2;i++){
        s_request[i]=MPI_REQUEST_NULL;
	r_request[i]=MPI_REQUEST_NULL;
    }

    for (int j=0;j<MESSAGE_SIZE;j++){
		buffer_1[j]=5678+(j%10);
                buffer_2[j]=8765+(j%10);
    		r_buffer_1[j]=0;
		r_buffer_2[j]=0;  
    }


    tag_1=1234;tag_2=4321;
    
    std::vector<int> m_target(NUM_PARTNERS-1, 0);  //3 targets when total 4 ranks
    for (int i=0, j=0;i<NUM_PARTNERS;i++)
	if (i!=myid)
		m_target[j++]=i; 

std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
for(int iter=0;iter<NUM_ITERATIONS;iter++){

    dmpi_start_send_region();
    for (int i=0;i<m_target.size();i++){
	int dest = m_target[i];
    	MPI_Isend(&buffer_1[0], MESSAGE_SIZE,MPI_INT,dest, tag_1, MPI_COMM_WORLD, s_request+i);
	MPI_Isend(&buffer_2[0], MESSAGE_SIZE,MPI_INT,dest, tag_2, MPI_COMM_WORLD, s_request+ i+ m_target.size());
    }
    dmpi_end_send_region();

    for (int i=0;i<m_target.size();i++){
	int dest = m_target[i];
	MPI_Irecv(&r_buffer_1[0], MESSAGE_SIZE, MPI_INT,dest,tag_1,MPI_COMM_WORLD,r_request+i);
        MPI_Irecv(&r_buffer_2[0], MESSAGE_SIZE, MPI_INT,dest,tag_2,MPI_COMM_WORLD, r_request+ i+ m_target.size());
    }
    
    MPI_Waitall(m_target.size()*2, s_request, s_stat);
    MPI_Waitall(m_target.size()*2, r_request, r_stat);

    MPI_Barrier(MPI_COMM_WORLD );
#ifdef DEBUG_PRINT

if (myid==0){
    printf("buffer_1[]=\n");
    	for (int j=0;j<MESSAGE_SIZE;j++)
		printf("%d ", buffer_1[j]);
    printf("\n");

    printf("buffer_2[]=\n");
        for (int j=0;j<MESSAGE_SIZE;j++)
                printf("%d ", buffer_2[j]);
    printf("\n");
}
    MPI_Barrier(MPI_COMM_WORLD );

if (myid==1){
    printf("Here is Rank %d, r_buffer_1[]=\n", myid);
        for (int j=0;j<MESSAGE_SIZE;j++)
                printf("%d ", r_buffer_1[j]);
    printf("\n");
    printf("Here is Rank %d, r_buffer_2[]=\n", myid);
        for (int j=0;j<MESSAGE_SIZE;j++)
                printf("%d ", r_buffer_2[j]);
    printf("\n");
}
    MPI_Barrier(MPI_COMM_WORLD );

#endif

}

    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    std::cout << "The execution time is "<<duration<<"\n";
    
    MPI_Finalize();
}
