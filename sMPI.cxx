#include "mpi.h"

#define GNTHOR_INTERNAL 1

#include "DMPI/sMPI.h"
#include "DMPI/DMPI.h"
#include "DMPI/datatypes.h"
#include "DMPI/PeerQueue.h"
#include "DMPI/Reordering.h"

DMPI::PeerQueue g_myqueue;
DMPI::Reordering myReordering;   //Only for GlobalScedule
/*
int sMPI_Init( int *argc, char ***argv ){
	printf("Test the shim\n");
	MPI_Init(&argc, &argv);
	//MPI_Init(NULL, NULL);
}*/

int sMPI_Comm_size( MPI_Comm comm, int *size ) {
	MPI_Comm_size(comm, size );
	//printf ("There are totally %d ranks\n", *size);
	myReordering.NUM_PARTNERS= *size;  //for future extend, record all ranks in system
	//myReordering.Initialize();
}

int sMPI_Comm_rank( MPI_Comm comm, int *rank ) {
	//printf ("There are totally %d ranks\n", myReordering.NUM_PARTNERS);
	MPI_Comm_rank(comm, rank);
	//printf("My rank id is %d", *rank);	
	myReordering.RankID = *rank;
	myReordering.Initialize();
}

int sMPI_Isend(const void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request){
	g_myqueue.enqueue_request(SEND_MESSAGE, (void*) buf, count, datatype, dest, tag, comm, request);
}

int sMPI_Irecv(void *buf, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm comm, MPI_Request *request){
	/*Reordering recv message*/
	//g_myqueue.enqueue_request(RECV_MESSAGE, buf, count, datatype, source, tag, comm, request);
	
	/*Only reorder the send message. All the recv message is issued normally. NO Reordering for recv message*/
	MPI_Irecv(buf, count, datatype, source, tag, comm, request);
}

int sMPI_Wait(MPI_Request *request, MPI_Status *status){
	MPI_Wait(request,status);
        //myReordering.issue_request_bundled(g_myqueue);
	//myReordering.request_completion(request,status);
	//printf("the MPI_Wait result is %d\n", temp);
}

int sMPI_Waitall(int count, MPI_Request array_of_requests[],MPI_Status array_of_statuses[]){
	//move this function to dmpi_end_send_region
	//myReordering.issue_request_bundled(g_myqueue);

	myReordering.request_completion(count, array_of_requests,array_of_statuses);
}

int sMPI_Barrier( MPI_Comm comm ){
	myReordering.Analysis(true);
	MPI_Barrier(comm );
}
